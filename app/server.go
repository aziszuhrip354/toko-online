package app

import(
	"gorm.io/gorm"
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

type Server struct {
	DB 		*gorm.DB
	Rourer 	*mux.Router
}

func main() {
	var server = Server{}
	const port = ":3000"

	server.Initialize()
	server.run(port)
}

func (server *Server) Initialize() {
	fmt.Println("Welcome to toko online")

	server.Router = mux.NewRouter()
}

func (server *Server) Run(addr string) {
	fmt.Printf("Listening tp port: %s", addr)
	log.Fatal(http.ListenAndServe(addr, server.Router))
}